#!/usr/bin/python
import paramiko
import smtplib

#Temporary values used until SSH keys are implemented
serverPassword = 'ZoSoiA9320!'
serverPasswordNew = 'tes7U3heD2evaFEm'
serverPasswordCP1 = 'n8ChecEjAnEDajut'

#Create a list to store all the issues found
serverIssues = []
lineCount = 0
mailCount = {}
#Setup the needed shizzle for email to work
smtpSender = 'dev@exile-tek.com'
smtpReceiver = 'daniel.longshaw@m247.com'

#Setup the SSH connection to the needed servers
ssh_cp1 = paramiko.SSHClient()
ssh_cp1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_cp1.connect('cp1.000025.net', password=serverPasswordCP1,  port=8752, username='root')
except:
  print("Authentication error on Server: CP1")
  quit()

ssh_rweb1 = paramiko.SSHClient()
ssh_rweb1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_rweb1.connect('rweb1-v.000025.net', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Rweb1")
  quit()

ssh_rweb2 = paramiko.SSHClient()
ssh_rweb2.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_rweb2.connect('rweb2-v.000025.net', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Rweb2")
  quit()

ssh_rweb3 = paramiko.SSHClient()
ssh_rweb3.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_rweb3.connect('rweb3-v.000025.net', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Rweb3")
  quit()

ssh_rweb4 = paramiko.SSHClient()
ssh_rweb4.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_rweb4.connect('rweb4-v.000025.net', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Rweb4")
  quit()

ssh_rweb5 = paramiko.SSHClient()
ssh_rweb5.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_rweb5.connect('rweb5-v.000025.net', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Rweb5")
  quit()

ssh_sweb3 = paramiko.SSHClient()
ssh_sweb3.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_sweb3.connect('sweb3-v.primehosting.co.uk', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Sweb3")
  quit()

ssh_sweb1 = paramiko.SSHClient()
ssh_sweb1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_sweb1.connect('sweb1-v.primehosting.co.uk', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Sweb1")
  quit()

ssh_sweb2 = paramiko.SSHClient()
ssh_sweb2.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_sweb2.connect('sweb2-v.primehosting.co.uk', password=serverPassword,  port=8752, username='root')
except:
  print("Authentication error on Server: Sweb2")
  quit()

ssh_sweb4 = paramiko.SSHClient()
ssh_sweb4.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
  ssh_sweb4.connect('213.230.203.98', password=serverPasswordNew,  port=8752, username='root')
except:
  print("Authentication error on Server: Sweb2")
  quit()

#Lets delete all bounceback's and get the spool count of each server
print "CP1:"
stdin, stdout, stderr = ssh_cp1.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on CP1 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_cp1.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "CP1 Mail spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on CP1 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_cp1.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("CP1 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()

print "Rweb1:"
stdin, stdout, stderr = ssh_rweb1.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Rweb1 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_rweb1.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Rweb1 Mail spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Rweb1 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_rweb1.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Rweb1 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Rweb2:"
stdin, stdout, stderr = ssh_rweb2.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Rweb2 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_rweb2.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Rweb2 Mail spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Rweb2 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_rweb2.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Rweb2 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Rweb3:"
stdin, stdout, stderr = ssh_rweb3.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Rweb3 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_rweb3.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Rweb3 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Rweb3 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_rweb3.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Rweb3 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Rweb4:"
stdin, stdout, stderr = ssh_rweb4.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Rweb4 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_rweb4.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Rweb4 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Rweb4 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_rweb4.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Rweb4 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Rweb5:"
stdin, stdout, stderr = ssh_rweb5.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Rweb5 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_rweb5.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Rweb5 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Rweb5 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_rweb5.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c |sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Rweb5 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Sweb1:"
stdin, stdout, stderr = ssh_sweb1.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Sweb1 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_sweb1.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Sweb1 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Sweb1 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_sweb1.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Sweb1 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Sweb2:"
stdin, stdout, stderr = ssh_sweb2.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Sweb2 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_sweb2.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Sweb2 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Sweb2 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_sweb2.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Sweb2 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Sweb3:"
stdin, stdout, stderr = ssh_sweb3.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Sweb3 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_sweb3.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Sweb3 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Sweb3 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_sweb3.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Sweb3 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
print "Sweb4:" 
stdin, stdout, stderr = ssh_sweb4.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
for line in iter(stdout.readline, ''):
  lineCount = lineCount + 1
if lineCount > 1000:
  serverIssues.append("Mail bouncebacks on Sweb4 are above the recommended limit at: " + lineCount + ", Somebody may be spamming")
print ("Bounceback + frozen messages deleted: " + str(lineCount))
lineCount = 0
stdin, stdout, stderr = ssh_sweb4.exec_command('exim -bpc')
for line in iter(stdout.readline, ''):
  print "Sweb4 spool count: " + line
  if int(line) > 1000:
    serverIssues.append("Mail spool count on Sweb4 is above the recommended limit: " + line)
    #Mail count is above 1000, Lets find out who's spamming
    stdin, stdout, stderr = ssh_sweb4.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
    for line in iter(stdout.readline, ''):
      l = line.split()
      try:
        mailCount.update({l[0]:l[1]})
      except:
        pass
    for i,j in mailCount.items():
      print i,j
      if int(i) > 200:
        serverIssues.append("Sweb4 spammer: " + j + " with " + i + " messages in the queue, nuke their account.")
    mailCount.clear()
#Finally lets print out the issues and prepare the email
for item in serverIssues:
  print item

#Send the email containing all the issues in serverIssues
if len(serverIssues) == 0:
  #No items in list
  print "No issues at present, Congratulations"
else:
  message = 'Server issues on primehosting at present are: \n \n'
  for item in serverIssues:
    message = message + item + '\n'
  message = message + 'Please ensure these above items are taken care of or this email will continue to spam your inbox \n'
  message = message + '\n \n The primehosting spam assasin, Always at your service'
  smtpObj = smtplib.SMTP('mail.exile-tek.com')
  smtpObj.login('dev@exile-tek.com', '27433003')
  smtpObj.sendmail(smtpSender, smtpReceiver, message)
  print "Successfully sent email"
