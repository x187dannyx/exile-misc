import salt.client
import MySQLdb
from random import randrange

class HostingDB:
	host = "188.226.195.28"
	user = "hosting"
	passwd = "ALLy0Ur8A53AR383l0NG70U2"
	db = "hosting"

	def __init__(self):
		self.connection = MySQLdb.connect( host = self.host,
						   user = self.user,
						   passwd = self.passwd,
						   db = self.db )
	def query(self, q):
		cursor = self.connection.cursor()
		cursor.execute(q)
		return cursor.fetchone()

	def __del__(self):
		self.connection.close()

exitvariable = 0

if __name__ == "__main__":
	while True:
		HostingQuery = HostingDB()
		client = salt.client.LocalClient()
		
		q = ("SELECT type, service_id, hosting, hostingenabled, hostingtype, domain from queue_runner INNER JOIN domains ON services.domainid=domains.id INNER JOIN services ON queue_runner.service_id=queue_runner.service_id")
		QueueRunner = HostingQuery.query(q)

		QueueRunner_type = QueueRunner[0]
		QueueRunner_serviceid = QueueRunner[1]
		QueueRunner_domain = QueueRunner[5]
		QueueRunner_hostingtype = QueueRunner[4]
		if QueueRunner_type == 'fail':
                	break
		if QueueRunner_type == 'hosting':
			if QueueRunner[3] == '1':
				print("Error, Hosting already exists on this domain")
			
			if QueueRunner[3] == '0' and QueueRunner[2] == '0':
				if QueueRunner_hostingtype == 'linux':
					q = ("SELECT saltaddress, id  FROM servers WHERE serviceusage < servicelimit AND active = '1' AND type = 'hosting-linux' ORDER BY RAND() LIMIT 1")
					HostingResult = HostingQuery.query(q)
					minion = HostingResult[0]
			
					saltcmd = ('echo -e "<VirtualHost 10.200.1.5:80> \n\tServerName ' + QueueRunner_domain + '\n\tServerAdmin daniel.longshaw@m247.com \n\tDocumentRoot /home/' + QueueRunner_domain + '/public_html \n\tErrorlog /home/' + QueueRunner_domain + '/logs/error.log \n\tCustomlog /home/' + QueueRunner_domain + '/logs/access.log combined \n</VirtualHost>" >> /etc/apache2/sites-enabled/' + QueueRunner_domain)
					client.cmd(minion, 'cmd.run', [saltcmd])

					saltcmd = ('a2ensite ' + QueueRunner_domain)
					Client.cmd(minion, 'cmd.run', [saltcmd])

					saltcmd = ('mkdir /home/' + QueueRunner_domain + ' && mkdir /home/' + QueueRunner_domain + '/logs && mkdir /home/' + QueueRunner_domain + '/public_html && mkdir /home/' + QueueRunner_domain + '/tmp && chmod 777 /home/' + QueueRunner_domain + 'tmp')
					Client.cmd(minion, 'cmd.run', [saltcmd])

					saltcmd = ('useradd ' + QueueRunner_domain)
					client.cmd(minion, 'cmd.run', [saltcmd])
					password = 'test'
			
					saltcmd = ('echo -e "' + password + '\n' + password + '" | passwd ' + QueueRunner_domain)
					client.cmd(minion, 'cmd.run', [saltcmd])
					
					saltcmd = ('service apache2 restart')
					Client.cmd(minion, 'cmd.run', [saltcmd])			
        	        	        #All server-side setup complete, Write to DB, increase the usage, and mark the hosting as active
					
					
        		       	        #TODO: Implement the private/public ZONE checking here

	                       		#TODO: Use salt to implement hosting, VHOST, activate site, configure user and CHROOT FTP
        	               		#TODO: Report back result to the MySQL DB

					break
		
