#!/usr/bin/python
from subprocess import call
from blessings import Terminal
from netaddr import IPNetwork, IPAddress
import sys
import socket

t = Terminal()

#Grab domain
#domainName = raw_input("Enter domain name: ")
domainName = sys.argv[1]

try:
  domainIP = socket.gethostbyname(domainName)
except:
  domainIP = ""

if domainIP == "89.238.188.26":
  domainIP = "MySQL1"
elif domainIP == "89.238.188.44":
  domainIP = "MySQL2"
elif domainIP == "89.238.188.80":
  domainIP = "Reflector1 Preview"
elif domainIP == "89.238.188.79":
  domainIP = "Reflector1 Shared SSL"
elif domainIP == "89.238.188.188":
  domainIP = "Reflector1 Shared Webspace"
elif domainIP == "89.238.188.46":
  domainIP = "MSSQL1"
elif domainIP == "89.238.188.76":
  domainIP = "MySQL3"
elif domainIP == "89.238.188.78":
  domainIP = "MySQL4"
elif domainIP == "89.238.188.22":
  domainIP = "Apache1"
elif domainIP == "89.238.188.24":
  domainIP = "Apache2"
elif domainIP == "89.238.188.39":
  domainIP = "Apache3"
elif domainIP == "89.238.188.42":
  domainIP = "Apache4"
elif domainIP == "89.238.188.5":
  domainIP = "IIS1"
elif domainIP == "89.238.188.29":
  domainIP = "IIS2"
elif domainIP == "89.238.188.48":
  domainIP = "IIS3"
elif domainIP == "89.238.188.50":
  domainIP = "IIS4"
elif domainIP == "89.238.188.82":
  domainIP = "IIS5"
elif domainIP == "89.238.188.84":
  domainIP = "IIS6"
elif domainIP == "89.238.188.86":
  domainIP = "IIS7"
elif domainIP == "89.238.188.89":
  domainIP = "IIS8"
elif domainIP == "89.238.188.91":
  domainIP = "IIS9"
elif domainIP == "89.238.188.93":
  domainIP = "IIS10"
elif domainIP == "89.238.188.95":
  domainIP = "IIS11"
elif domainIP == "89.238.188.97":
  domainIP = "IIS12"
elif domainIP == "89.238.188.99":
  domainIP = "IIS13"
elif domainIP == "89.238.188.112":
  domainIP = "IIS14"
elif domainIP == "89.238.188.114":
  domainIP = "IIS15"
elif domainIP == "89.238.188.116":
  domainIP = "IIS16"
elif domainIP == "89.238.188.118":
  domainIP = "IIS17"
elif domainIP == "89.238.188.120":
  domainIP = "IIS18"
elif domainIP == "89.238.188.122":
  domainIP = "IIS19"
elif domainIP == "89.238.188.124":
  domainIP = "IIS20"
elif domainIP == "89.238.188.126":
  domainIP = "IIS21"
elif domainIP == "89.238.188.128":
  domainIP = "IIS22"
elif domainIP == "89.238.188.130":
  domainIP = "IIS23"
elif domainIP == "89.238.188.178":
  domainIP = "Managed1"
elif domainIP == "89.238.188.168":
  domainIP = "Premium1"
elif domainIP == "89.238.188.170":
  domainIP = "Premium2"
else:
  try:
    if IPAddress(domainIP) in IPNetwork("89.238.188.0/24"):
      domainIP = "New Hosting, Server Unknown"
  except:
   domainIP = domainIP

print("")

print t.red("Nameservers are showing as: ")
shellCommand = "dig NS +short " + domainName
call(shellCommand, shell=True)

print("")

print t.red("A Record M247 see\'s as: ")
shellCommand = "dig A +short @a.ns.ns247.net +time=1 +tries=1 " + domainName
call(shellCommand, shell=True)

print t.red("www A Record M247 see\'s as: ")
shellCommand = "dig A +short @a.ns.ns247.net +time=1 +tries=1 www." + domainName
call(shellCommand, shell=True)

print("")

print t.red("A Record Redfox see\'s as: ")
shellCommand = "dig A +short +time=2 +tries=1 @ns1.realnameservers.com " + domainName
call(shellCommand, shell=True)

print t.red("www A Record Redfox see\'s as: ")
shellCommand = "dig A +short +time=2 +tries=1 @ns1.realnameservers.com www." + domainName
call(shellCommand, shell=True)

print("")

print t.red("A Record Google see as: ")
shellCommand = "dig A +time=1 +tries=1 +short @8.8.8.8 " + domainName
call(shellCommand, shell=True)

print t.red("www A Record Google see as: ")
shellCommand = "dig A  +time=1 +tries=1 +short @8.8.8.8 www." + domainName
call(shellCommand, shell=True)

print("")

print t.red("Trace is as: ")
shellCommand = "traceroute -m 8 " + domainName
call(shellCommand, shell=True)

print("")

if domainIP != "":
  print t.red("Domain resides on: " + t.green(domainIP))
else:
  print t.red("Domain could not be resolved")
