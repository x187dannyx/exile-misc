#
#TODO
#---
#	Check if needed trailing slash has been added to folder to upload
#	Add ability to pull files from another server
#"""

from subprocess import call

def server_check(server):
	if server == "apache1":
		dest_server = "http.apache1.hosting.m247.com"
	elif server == "apache2":
        	dest_server = "http.apache2.hosting.m247.com"
	elif server == "apache3":
        	dest_server = "http.apache3.hosting.m247.com"
	elif server == "apache4":
        	dest_server = "http.apache4.hosting.m247.com"
	elif server == "apache5":
        	dest_server = "http.apache5.hosting.m247.com"
	elif server == "iis1":
        	dest_server = "http.iis1.hosting.m247.com"
	elif server == "iis2":
        	dest_server = "http.iis2.hosting.m247.com"
	elif server == "iis3":
        	dest_server = "http.iis3.hosting.m247.com"
	elif server == "iis4":
        	dest_server = "http.iis4.hosting.m247.com"
	elif server == "iis5":
        	dest_server = "http.iis5.hosting.m247.com"
	elif server == "iis6":
        	dest_server = "http.iis6.hosting.m247.com"
	elif server == "iis7":
        	dest_server = "http.iis7.hosting.m247.com"
	elif server == "mysql1":
		dest_server = "mysql.mysql1.hosting.m247.com"
	elif server == "mysql2":
		dest_server = "mysql.mysql2.hosting.m247.com"
	elif server == "mysql3":
		dest_server = "mysql.mysql3.hosting.m247.com"
	elif server == "mysql4":
		dest_server = "mysql.mysql4.hosting.m247.com"
	else:
        	dest_server = dest_server

#Lets begin with inputs needed

ftp_upload = raw_input("Are we uploading a website? (Y/N): ")
#If FTP is needed then ask for details
if ftp_upload == "Y" or ftp_upload == "y":
	ftp_server = raw_input("Enter destination FTP Server: ")
	ftp_username = raw_input("Enter FTP Usernaem: ")
	ftp_password = raw_input("Enter FTP Password: ")
	original_file = raw_input("Enter .tar.gz Filename or URL to tar.gz file: ")

database_upload = raw_input("Are we uploading a MySQL database? (Y/N): ")
#If Database is needed then ask for details
if database_upload == "Y" or database_upload == "y":
	database_server = raw_input("Enter MySQL Server Address: ")
	database_user = raw_input("Enter MySQL Username: ")
	database_passwrod = raw_input("Enter MySQL Password: ")
	database_datbase = raw_input("Enter MySQL Database: ")
	database_dump = raw_input("Enter .sql Filename to upload: ")

#Time to unzip if a .tar.gz file was given
if ".tar.gz" in original_file:
	#Create directory if not exists
	shell_command = "mkdir /tmp/ftp_extract"
	call(shell_command, shell=True)
	
	#Extract archive to the directory created
	shell_command = "tar -xzvf " + original_file + " -C /tmp/ftp_extract"
	call(shell_command, shell=True)
	extracted = 1

print "Time to FTP the files to the new server ... "
ftp_server = server_check(ftp_server)
#Do command based on if we extracted files or not, If extracted the directory would be different
if extracted == 1:
	#FTP the files from the ftp_extract directory
	shell_command = "ncftpput -R -v -u " + ftp_username + " -p " + ftp_password + " " + ftp_server + " /public_html/ /tmp/ftp_extract/*"
	call(shell_command, shell=True)

	#Delete the temporary directory
	shell_command = "rm -rf /tmp/ftp_extract"
	call(shell_command, shell=True)
else:
	#Upload files from the directory given
	shell_command = "ncftpput -R -v -u " + ftp_username + " -p " + ftp_password + " " + ftp_server + " /public_html/ " + original_file
	call(shell_command, shell=True)
