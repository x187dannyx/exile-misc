#!/usr/bin/python
import paramiko
import smtplib
import time

serverList = {'Rweb1-v': 'rweb1-v.000025.net', 'Rweb2-v': 'rweb2-v.000025.net', 'Rweb3-v': 'rweb3-v.000025.net', 'Rweb4-v': 'rweb4-v.000025.net',
              'Rweb5-v': 'rweb5-v.000025.net', 'Sweb1-v': 'sweb1-v.primehosting.co.uk', 'sweb2-v': 'sweb2-v.primehosting.co.uk', 'sweb3-v': 'sweb3-v.primehosting.co.uk',
              'Sweb4': '213.230.203.98', 'CP1': 'cp1.000025.net', 'CP2': 'cp2.000025.net', 'CP3': 'cp3.000025.net', 'CP4': 'cp4.000025.net', 'CP5': 'cp5.000025.net'}

#Temporary values used until SSH keys are implemented
serverPassword = 'ZoSoiA9320!'
serverPasswordNew = 'tes7U3heD2evaFEm'
serverPasswordCP1 = 'n8ChecEjAnEDajut'

#Create a list to store all the issues found
issueList = []
lineCount = 0
spammerDict = {}
#Setup the needed shizzle for email to work
smtpSender = 'dev@exile-tek.com'
smtpReceiver = ['daniel.longshaw@m247.com','morgan.howarth@m247.com']

while True:
  #Loop through the serverList and do the needful
  for serverName,serverAddress in serverList.items():
    #Make the ssh connection
    serverConnection = paramiko.SSHClient()
    serverConnection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
      serverConnection.connect(serverAddress, port=8752, username='root')
      print('Now working on server: ' + serverName)
      sdtin, stdout, stderr = serverConnection.exec_command("exim -bp | grep -E '^ ?[0-9]' | grep '<>' | awk '{ print $3 }' | xargs exim -Mrm")
      for line in iter(stdout.readline, ''):
        lineCount = lineCount + 1
      if lineCount > 1000:
        issueList.append('Mail bouncebacks on ' + serverName + ' are above the recommended limit at: ' + lineCount + ', Somebody is probably spamming!')
      print('Bounceback + Frozen messages deleted: ' + str(lineCount))
      lineCount = 0

      #Now we need to check who is spamming if the mail Queue is over a defined limit
      stdin, stdout, stderr = serverConnection.exec_command("exim -bpc")
      for line in iter(stdout.readline, ''):
        print('Server: ' + serverName + ' spool count is at: ' + str(line))
        if int(line) > 1000:
          issueList.append('Mail spool count on ' + serverName + ' is above the recommended limit at: ' + str(line))
          #Now we need to check who is making the spool count > than the defined limit
          stdin, stdout, stderr = serverConnection.exec_command("exim -bpr | grep '<*@*>' | awk '{print $4}'|grep -v '<>' | sort | uniq -c | sort -rn")
          for line in iter(stdout.readline, ''):
            l = line.split()
            try:
              spammerDict.update({l[0]: l[1]})
            except:
              pass
            for count, spammer in spammerDict.items():
              print(count + ' Messages by user: ' + spammer)
              if int(count) > 200:
                issueList.append('Spammer on server: ' + serverName + ', email address: ' + spammer + 'with ' + count + ' messages in the queue, nuke their account pl0x')
            spammerDict.clear()
      serverConnection.close()
    except:
      print('Authentication failure on server: ' + serverName)
  #Print all items and prepare to send the email out
  for issue in issueList:
    print issue
  #Send the email containing all the issues in serverIssues
  if len(serverIssues) == 0:
    #No items in list
    print "No issues at present, Congratulations"
  else:
    message = 'Server issues on primehosting at present are: \n \n'
    for item in serverIssues:
      message = message + item + '\n'
    message = message + 'Please ensure these above items are taken care of or this email will continue to spam your inbox \n'
    message = message + '\n \n The primehosting spam assasin, Always at your service'
    smtpObj = smtplib.SMTP('mail.exile-tek.com')
    smtpObj.login('dev@exile-tek.com', '27433003')
    smtpObj.sendmail(smtpSender, smtpReceiver, message)
    print "Successfully sent email"

  #Sleep the loop for half hour or so
  print('')
  print('Going to sleep for a little nap ...')
  print('')
  time.sleep(1800)